namespace DiscosRESTful.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Album")]
    public partial class Album
    {
        public int id { get; set; }

        [Required]
        [StringLength(100)]
        public string artist { get; set; }

        [Required]
        [StringLength(500)]
        public string title { get; set; }

        [Column(TypeName = "date")]
        public DateTime? releaseDate { get; set; }
    }
}
