namespace DiscosRESTful.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AlbumModel : DbContext
    {
        public AlbumModel()
            : base("name=AlbumModel")
        {
        }

        public virtual DbSet<Album> Album { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Album>()
                .Property(e => e.artist)
                .IsUnicode(false);

            modelBuilder.Entity<Album>()
                .Property(e => e.title)
                .IsUnicode(false);
        }
    }
}
