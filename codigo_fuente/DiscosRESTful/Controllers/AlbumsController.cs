﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using DiscosRESTful.Models;
using Newtonsoft.Json.Linq;

namespace DiscosRESTful.Controllers
{
    public class AlbumModelsController : ApiController
    {
        private AlbumModel db = new AlbumModel();

        // GET: api/AlbumModels
        /// <summary>
        /// Loads a list containint all Albums from database in XML format.
        /// </summary>
        /// <returns>A list with all Albums as XML.</returns>
        public IQueryable<Album> GetDisks()
        {
            return db.Album;
        }

        // GET: api/AlbumModels/5
        /// <summary>
        /// Loads the Album whose id equals {id} and returns its XML, or BadRequest if id doesn't match with any Album.
        /// </summary>
        /// <param name="id">The Album id.</param>
        /// <returns>The Album as a XML.</returns>
        [ResponseType(typeof(Album))]
        public IHttpActionResult GetAlbumModel(int id)
        {
            Album album = db.Album.Find(id);
            if (album == null)
            {
                return NotFound();
            }
            return Ok(album);
        }

        // PUT: api/AlbumModels/5
        /// <summary>
        /// Modifies the Album whose id equals {id} and returns its XML, or BadRequest if id doesn't match with any Album.
        /// </summary>
        /// <param name="id">The Album id.</param>
        /// <param name="album">The Album object specified in the request.</param>
        /// <returns>The Album as a XML.</returns>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlbumModel(int id, Album album)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != album.id)
            {
                return BadRequest();
            }

            db.Entry(album).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlbumModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AlbumModels
        /// <summary>
        /// Creates a new Album as specified in the request, and stores it in the database.
        /// </summary>
        /// <param name="album">The Album object specified in the request.</param>
        /// <returns>The Album as a XML.</returns>
        [ResponseType(typeof(Album))]
        public IHttpActionResult PostAlbumModel(Album album)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Album.Add(album);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = album.id }, album);
        }

        // DELETE: api/AlbumModels/5
        /// <summary>
        /// Deletes the Album whose id equals {id} and returns its XML, or BadRequest if id doesn't match with any Album.
        /// </summary>
        /// <param name="id">The Album id.</param>
        /// <returns>The Album as a XML.</returns>
        [ResponseType(typeof(Album))]
        public IHttpActionResult DeleteAlbumModel(int id)
        {
            Album album = db.Album.Find(id);
            if (album == null)
            {
                return NotFound();
            }

            db.Album.Remove(album);
            db.SaveChanges();

            return Ok(album);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AlbumModelExists(int id)
        {
            return db.Album.Count(e => e.id == id) > 0;
        }
    }
}